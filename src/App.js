
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Routes, Navigate } from "react-router-dom";
// import './styles/global.scss';
import "./styles/global.scss"
import Layout from './components/layout';
import Dashboard from './pages/dashboard';
import Customers from './pages/customers';
import Employee from './pages/employee';
import EditProduct from './pages/editProducts';
import Coupon from './pages/coupon';
import SalesAnalytics from './pages/saleAnalytics';
import Settings from './pages/settings';





function App() {

  return (
    <>
      <Layout>
        <Routes>
          <Route path='/' element={<Navigate to="/dashboard" />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/employee' element={<Employee />} />


          <Route path='/coupon' element={<Coupon />} />


          <Route path='/customers' element={<Customers />} />


          <Route path='/edit-products' element={<EditProduct />} />



          <Route path='/sale-analytics' element={<SalesAnalytics />} />


          <Route path='/settings' element={<Settings />} />



        </Routes>
      </Layout>
    </>
  );
}

export default App;
