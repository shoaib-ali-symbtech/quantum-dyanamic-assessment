import React, { memo } from "react";
import ShiftCard from "./shift_card";

function ShiftComponent({ shift }) {
    return <div className="bg-white rounded mt-2 px-2 py-3">
        <div className="d-flex justify-content-between align-items-center flex-wrap mx-1 mx-md-2">
            <h3 className="loc-color-primary loc-text-medium fw-bold">Shift ({shift.shift_time})</h3>
            <div className="d-flex align-items-center">
                <img src="icons/pen.svg" type="button" className="me-4" alt="pen-edit" />
                <img src="icons/plus.svg" type="button" alt="add" />
            </div>
        </div>
        <div className="row mx-0">
            {
                shift?.employees?.map(v => <div className="col-12 col-md-6 col-lg-4 mt-4" key={v.id}>
                    <ShiftCard details={v} />
                </div>)

            }
        </div>
    </div>
}

export default memo(ShiftComponent)