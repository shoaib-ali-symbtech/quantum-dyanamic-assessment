import React, { memo } from "react";

function AnalyticsCard({ title, number, keyvalue }) {
    return <div className="shadow rounded py-2 px-3 bg-white">
        <div className="d-flex justify-content-between">
            <h2 className="loc-color-primary loc-text-medium fw-bold">{title}</h2>
            {number > 0 ? <p className="m-0 p-0 loc-color-secondary">&#x2191;</p> : <p className=" text-danger">&#x2193;</p>}
        </div>
        <div className="d-flex justify-content-between">
            <h2 className="loc-color-primary loc-text-medium fw-bold">{keyvalue}</h2>
            <p className={`${number > 0 ? " p-0 m-0 loc-color-secondary" : "text-danger"} loc-text-medium fw-bold`}> {number}%</p>
        </div>
    </div>
}

export default memo(AnalyticsCard);