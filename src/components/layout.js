import React from "react";
import { Link, useLocation } from "react-router-dom";

export default function Layout({ children }) {
    const location = useLocation();
    return <div className="layout" >

        <header className="d-flex px-3 py-3 shadow  align-items-center flex-wrap border-bottom">
            <div className="col ">
                <div className="d-flex align-items-center">
                    <div className="p-4 loc-bg-lay-light rounded  " />
                    <div className="col ms-2 pt-2 ">
                        <h3 className="loc-text-xxlarge loc-color-primary fw-bold">Dazzie</h3>
                    </div>
                </div>
            </div>
            <div className="col ">
                <div className="d-flex  justify-content-end align-items-center ">
                    <img src={"icons/notification_bell.svg"} alt="bell icons" />
                    <div className="d-flex align-items-center me-0 me-md-5 ms-2">
                        <div className="p-3 rounded-circle loc-bg-lay-light" />
                        <div className="ms-2">
                            <h3 className="loc-text-medium p-0 m-0 loc-color-primary fw-bold">Sumanta</h3>
                            <p className="loc-text-xsmall p-0 m-0 loc-color-light fw-light">Cashier</p>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div className="d-flex col   p-0 m-0 main">
            <nav className="loc-nav bg-white shadow border-right">
                <ul className="menu d-flex flex-column justify-content-between h-100 overflow-auto">
                    <div>
                        <li className={`${location.pathname === "/dashboard" ? "selected" : ""}`}><Link to="/dashboard" ><img src={"icons/menu.svg"} alt="menu" /></Link></li>
                        <li className={`${location.pathname === "/sale-analytics" ? "selected" : ""}`}><Link to="/sale-analytics"><img src={"icons/graph.svg"} alt="graph" /></Link></li>
                        <li className={`${location.pathname === "/customers" ? "selected" : ""}`}><Link to="/customers"><img src={"icons/customer.svg"} alt="customer" /></Link></li>
                        <li className={`${location.pathname === "/edit-products" ? "selected" : ""}`}><Link to="/edit-products"><img src={"icons/package.svg"} alt="package" /></Link></li>
                        <li className={`${location.pathname === "/coupon" ? "selected" : ""}`}><Link to="/coupon"><img src={"icons/coupon_discount.svg"} alt="discount" /></Link></li>
                        <li className={`${location.pathname === "/employee" ? "selected" : ""}`}><Link to="/employee"><img src={"icons/user.svg"} alt="user" /></Link></li>
                    </div>
                    <li className={`justify-self-end ${location.pathname === "/settings" ? "selected" : ""}`}><Link to="/settings"><img src={"icons/settings.svg"} alt="setting" /></Link></li>

                </ul>
            </nav>

            <main className="col layout-child  loc-bg-lay-light  p-3 overflow-auto" style={{ height: "100%" }}>
                {children}

            </main>
        </div>


    </div>


}