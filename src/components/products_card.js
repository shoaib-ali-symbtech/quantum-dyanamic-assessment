import React, { memo } from "react";



function ProductCard({ productDetails }) {
    return <div className="p-2 border rounded product-card">
        <img className="w-100 rounded" src={productDetails.product_url} alt={`${productDetails.name}`} />
        <div className="mt-2">
            <h3 className="loc-text-medium fw-bold loc-color-primary">{productDetails.name}</h3>
            <h3 className="loc-text-medium fw-bold loc-color-secondary">$ {productDetails.price}</h3>
        </div>


    </div>
}
export default memo(ProductCard)