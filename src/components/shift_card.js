import React, { memo } from "react";

function ShiftCard({ details }) {

    const chipSelector = (role) => {

        switch (role) {
            case "admin":
                return "disable";
            case "chef":
                return "warning";
            case "waitress":
                return "primary";
            case "dishwasher":
                return "outline"
            case "cashier":
                return "success"
            default:
                return "primary"
        }
    }

    return <div className="border rounded p-2 py-3">
        <div className="d-flex justify-content-between flex-wrap">
            <img src={details.profile_image} height={50} width={50} className="rounded-circle" alt={details.name + " profile"} />
            <div className="mt-1 ms-2 col">
                <h3 className="loc-color-primary loc-text-medium fw-bold p-0 m-0 mb-2">{details.name}</h3>
                <div className="d-flex align-items-center flex-wrap">
                    <div className={`rounded px-3  chip ${chipSelector(details.role)}`}>
                        <p className="loc-text-small text-break">{details.role}</p>
                    </div>
                    <p type="button" className="loc-text-small  loc-color-lay-primary ms-2 fw-bold">Details</p>
                </div>
            </div>
            <img src="icons/threedots.svg" height={24} alt="menu" type="button" />
        </div>

    </div>
}

export default memo(ShiftCard);