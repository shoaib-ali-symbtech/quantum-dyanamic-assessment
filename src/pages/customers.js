import React, { useEffect, useState, useMemo } from "react";
import axios from "axios";
import { useNavigate, useLocation } from "react-router-dom";

export default function Customers() {

    const [search, setSearch] = useState("")
    const [customers, setCustomers] = useState([]);
    const navigate = useNavigate();
    const location = useLocation();


    const getOrders = () => {
        axios.get('/json/customers.json')
            .then((data) => {
                setCustomers(data.data.customers)
            })
            .catch((error) => console.error('Error posting data:', error));
    }

    useEffect(() => { getOrders(); }, [])

    useEffect(() => {
        const queryParams = new URLSearchParams(location.search);
        setSearch(v => queryParams.get("search") ?? v);

    }, [location.search])


    const pushParams = (search) => {
        const queryParams = new URLSearchParams({ search });
        navigate({ search: queryParams.toString() })
    }

    function formatDate(dateString) {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        const formattedDate = new Date(dateString).toLocaleDateString(undefined, options);
        return formattedDate;
    }


    let filterData = useMemo(() => {

        return customers?.filter(v => v.name?.toLowerCase().includes(search.toLowerCase()));

    }, [customers, search])

    return <>
        <h1 className="loc-text-xlarge loc-color-primary fw-bold mx-3 ">Customers</h1>
        <div className="m-1 m-md-2  py-3 bg-white shadow rounded">

            <div className="row m-0 align-items-center">
                <div className="loc-bg-lay-light border d-flex align-items-center col mx-2 rounded py-1 px-2 mx-md-4 ">
                    <img src="/icons/search.svg" alt="search" />
                    <input onChange={(e => {

                        pushParams(e.target.value);
                    })} className=" col  loc-bg-lay-light   border-0 outline-none " placeholder="seach here ..." />
                </div>
                <img src="icons/filter.svg" type="button" className="icon" alt="filter" />
                <img src="icons/threedots.svg" type="button" className="icon" alt="filter" />
            </div>

            <div className="table-responsive mx-4 mt-2">
                {filterData.length > 0 ? <table className="table  ">
                    <thead className="table-light" >
                        <tr className="bg-primary">
                            <th className="col-2 ">ID</th>
                            <th className="col">Name</th>
                            <th className="col-2">Join Date</th>

                            <th className="col-2">Total Visits</th>
                            <th className="col-2">Purchased Items</th>
                            <th className="col-2">Total Spend</th>
                        </tr>
                    </thead>
                    <tbody>

                        {

                            filterData?.map((v) =>

                                <tr key={v.id} className="fw-light loc-text-medium loc-color-primary">
                                    <td className="">XXX{v.id.toString().slice(v.id.toString().length - 2)}</td>
                                    <td className="">{v.name}</td>
                                    <td className="">{formatDate(v.join_date)}</td>
                                    <td className="">{v.total_visits}</td>
                                    <td className="">{v.purchase_items?.map(pi => `${pi.product_name} (${pi.quantity})`).join(",")}<br /><span type="button" className="loc-color-lay-primary fw-bold">See Details</span> </td>
                                    <td className="">$ {v.total_spend}</td>
                                </tr>


                            )
                        }

                    </tbody>
                </table> : <p className="text-center text-secondary  fw-bold my-5">No Customer found</p>}

            </div>
        </div>
    </>
}