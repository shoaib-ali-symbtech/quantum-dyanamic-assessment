import React, { useEffect, useState, useMemo } from "react";
import axios from "axios";
import { useNavigate, useLocation } from "react-router-dom";


export default function Coupon() {

    const [search, setSearch] = useState("")
    const [coupons, setCoupons] = useState([]);
    const navigate = useNavigate();
    const location = useLocation();


    const getCoupons = () => {
        axios.get('/json/coupons.json')
            .then((data) => {
                setCoupons(data.data)
            })
            .catch((error) => console.error('Error posting data:', error));
    }

    useEffect(() => { getCoupons(); }, [])

    useEffect(() => {
        const queryParams = new URLSearchParams(location.search);
        setSearch(v => queryParams.get("search") ?? v);

    }, [location.search])


    const pushParams = (search) => {
        const queryParams = new URLSearchParams({ search });
        navigate({ search: queryParams.toString() })
    }

    function formatDate(dateString) {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        const formattedDate = new Date(dateString).toLocaleDateString(undefined, options);
        return formattedDate;
    }


    let filterData = useMemo(() => {

        return coupons?.filter(v => v.code?.toLowerCase().includes(search.toLowerCase()));

    }, [coupons, search])

    return <>

        <div className="d-flex flex-wrap  justify-content-between align-items-center my-3">

            <h1 className="loc-text-xlarge loc-color-primary fw-bold mx-3 ">Coupons</h1>
            <button type="button" className="   rounded me-3 loc-bg-lay-primary text-white px-4 py-2 ">
                Create New
            </button>
        </div>
        <div className="m-1 m-md-2  py-3 bg-white shadow rounded">

            <div className="row m-0 align-items-center">
                <div className="loc-bg-lay-light border d-flex align-items-center col mx-2 rounded py-1 px-2 mx-md-4 ">
                    <img src="/icons/search.svg" alt="search" />
                    <input onChange={(e => {

                        pushParams(e.target.value);
                    })} className=" col  loc-bg-lay-light   border-0 outline-none " placeholder="seach here ..." />
                </div>
                <img src="icons/filter.svg" type="button" className="icon" alt="filter" />
                <img src="icons/threedots.svg" type="button" className="icon" alt="filter" />
            </div>

            <div className="table-responsive mx-4 mt-2">
                {filterData?.length > 0 ? <table className="table  ">
                    <thead className="table-light" >
                        <tr className="bg-primary">
                            <th className="col-1 ">ID</th>
                            <th className="col-2">Code</th>
                            <th className="col-2">Discount</th>

                            <th className="col">Start Date</th>
                            <th className="col">End Date</th>
                            <th className="col-1">Activate</th>
                            <th className="col-1"></th>

                        </tr>
                    </thead>
                    <tbody>

                        {

                            filterData?.map((v) =>

                                <tr key={v.id} className="fw-light loc-text-medium loc-color-primary">
                                    <td className="">{v.id}</td>
                                    <td className="">{v.code}</td>
                                    <td className="">{v.discount}%</td>
                                    <td className="">{formatDate(v.start_date)}</td>
                                    <td className="">{formatDate(v.end_date)} </td>
                                    <td className=" align-self-center">
                                        <div className="form-check form-switch">
                                            <input className="form-check-input" type="checkbox" defaultChecked={v.is_active} role="switch" id="flexSwitchCheckChecked" style={{ height: "1.5rem", minWidth: "2.5rem" }} />

                                        </div></td>
                                    <td>
                                        <img type="button" src="icons/threedots.svg" height={20} alt="filter" /></td>
                                </tr>


                            )
                        }

                    </tbody>
                </table> : <p className="text-center text-secondary  fw-bold my-5">No Customer found</p>}

            </div>
        </div>
    </>
}