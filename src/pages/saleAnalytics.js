import React, { useEffect, useState } from "react";
import AnalyticsCard from "../components/analytics_card";
import axios from "axios";
export default function SalesAnalytics() {

    const [orders, setOrders] = useState([]);
    const [itemsSold, setItemsSold] = useState([]);
    const getOrders = () => {
        axios.get('/json/order_list.json')
            .then((data) => {
                setOrders(data.data)
            })
            .catch((error) => console.error('Error posting data:', error));
    }

    const getSoldItems = () => {
        axios.get('/json/sold_items.json')
            .then((data) => {
                setItemsSold(data.data)
            })
            .catch((error) => console.error('Error posting data:', error));
    }

    useEffect(() => {
        getSoldItems();
        getOrders();

    }, [])


    return <div className=" h-100   px-2 overflow-auto d-lg-flex flex-column"  >
        {/* <div className="bg-danger" style={{ height: 150 }} /> */}
        <div className="row mt-3">
            <div className="col-lg col-md-6 col-12">
                <AnalyticsCard title={"Customers"} number={-15} keyvalue={1000} />
            </div>
            <div className="col-lg col-md-6 col-12 mt-lg-0 mt-2">
                <AnalyticsCard title={"Orders"} number={20} keyvalue={1050} />
            </div>
            <div className="col-lg col-md-6 col-12 mt-lg-0 mt-2">
                <AnalyticsCard title={"Revenue"} number={10} keyvalue={"$50.00"} />
            </div>
            <div className="col-lg col-md-6 col-12 mt-lg-0 mt-2">
                <AnalyticsCard title={"Net Profit"} number={12} keyvalue={"$12.00"} />
            </div>

        </div>

        <div className="row mt-4 mx-0  overflow-auto" style={{ flex: 1 }}>
            <div className="col-lg-8 col-12 p-0 mt-lg-0 mt-3   order-2 order-lg-1 pe-lg-2 rounded  " >

                <div className="  bg-white rounded p-3   w-100">

                    <div className="d-flex   justify-content-between border-bottom">
                        <h3 className="loc-text-medium loc-color-primary fw-bold mb-2 mx-0"  >Latest Orders</h3>

                    </div>



                    <div className="table-responsive">
                        <table className="table  ">
                            <thead className="table-light" >
                                <tr className="bg-primary">
                                    <th className="col-2 ">No.</th>
                                    <th className="col">Name</th>
                                    <th className="col-2">Quantity</th>

                                    <th className="col-2">Revenue</th>
                                    <th className="col-2">Net Profit</th>
                                </tr>
                            </thead>
                            <tbody>

                                {

                                    orders?.map((v, index) =>

                                        <tr key={v.id} className="fw-light loc-text-medium loc-color-primary">
                                            <td className="">{index.toString().padStart(2, "0")}</td>
                                            <td className="">{v.product}</td>
                                            <td className="">{v.quantity}</td>

                                            <td className="">{v.revenue}</td>
                                            <td className="">{v.net_profit}</td>
                                        </tr>


                                    )
                                }

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div className="col-lg-4 order-1 order-lg-2 col-12 h-100 bg-white pb-2 rounded  overflow-auto">
                <div className="d-flex m-2 justify-content-between border-bottom align-items-center py-2 flex-wrap">
                    <h3 className="loc-text-medium col loc-color-primary fw-bold" >Item Sold</h3>

                    <div className="col-7 col-md-4">
                        <select className="form-select border-primary loc-color-lay-primary fw-bold " >
                            <option selected>Best Seller</option>
                        </select>
                    </div>

                </div>

                {
                    itemsSold?.map(v => <div key={v.id} className="d-flex align-items-center mt-2">
                        <img src={v.image_url} height={50} width={50} className="rounded" alt={v.product_name} />
                        <h3 className=" col mx-2 text-truncate text-break text-wrap loc-text-medium loc-color-primary fw-bold">{v.product_name}</h3>
                        <p>{v.items_sold} items</p>
                    </div>)
                }
            </div>

        </div>

    </div >

}