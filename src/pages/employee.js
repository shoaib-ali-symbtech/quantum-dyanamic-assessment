import React, { useEffect, useState } from "react";
import ShiftComponent from "../components/shift_component";
import axios from "axios";
export default function Employee() {

    const [shifts, setShifts] = useState([])

    const getShift = () => {
        axios.get('/json/shifts.json')
            .then((data) => {
                setShifts(data.data.shifts)

            })
            .catch((error) => console.error('Error posting data:', error));
    }
    useEffect(() => {
        getShift();
    }, [])
    return <>
        <div className="d-flex flex-wrap  justify-content-between align-items-center my-3">
            <h1 className="loc-text-xlarge loc-color-primary fw-bold ">Employee</h1>
            <button type="button" className="   rounded me-3 loc-bg-lay-primary text-white px-4 py-2 ">
                Create New
            </button>
        </div>

        {
            shifts?.map(v => <div key={v.id}>
                <ShiftComponent shift={v} />
            </div>)
        }
    </>
}