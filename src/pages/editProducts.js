import React, { useState } from "react";

export default function EditProduct() {
    const [imageUrl, setImageUrl] = useState('');

    const handleInputChange = (event) => {
        const file = event.target.files[0];

        const reader = new FileReader();

        reader.onloadend = () => {
            setImageUrl(reader.result);
        };

        if (file) {
            reader.readAsDataURL(file);
        }
    };
    return <>
        <h1 className="loc-text-xlarge loc-color-primary fw-bold mx-3 "> Edit Product</h1>
        <div className="m-1 m-md-2  py-3 px-md-3 px-2 bg-white shadow rounded">
            <form>
                <div className="">
                    <label className="loc-text-medium loc-color-primary fw-bold ms-2">ID</label>
                    <input className="rounded border  form-control  p-2 mt-1 loc-bg-lay-light" />
                </div>


                <div className="mt-3 d-flex align-items-center flex-wrap">
                    {imageUrl !== "" ?
                        <img height={100} width={100} src={imageUrl} alt={"input image"} className="rounded loc-bg-lay-light me-3" />
                        :
                        <div className="rounded loc-bg-lay-light me-3 p-5" />
                    }
                    <div>
                        <label className="loc-text-medium loc-color-primary fw-bold my-2 ">Select your product picture</label><br />
                        <input onChange={handleInputChange} type="file" id="fileInput" accept=".png, .jpeg, .jpg" />
                        <label for="fileInput" id="customLabel">
                            <span>Browse</span>
                        </label>
                    </div>
                </div>



                <div className="mt-3">
                    <label className="loc-text-medium loc-color-primary fw-bold ms-2">Product Name</label>
                    <input className="rounded border  form-control  p-2 mt-1 loc-bg-lay-light" />
                </div>

                <div className="mt-3">
                    <label className="loc-text-medium loc-color-primary fw-bold ms-2">Category</label>
                    <select class="form-select loc-bg-lay-light" id="floatingSelect" >
                        <option selected>Category ...</option>
                        <option value="1">Food</option>
                        <option value="2">Drink</option>
                        <option value="3">Package</option>
                    </select>
                </div>


                <div className="mt-3">
                    <label className="loc-text-medium loc-color-primary fw-bold ms-2">Price</label>
                    <input className="rounded border  form-control  p-2 mt-1 loc-bg-lay-light" type="number" />
                </div>

                <div className="mt-3">
                    <label className="loc-text-medium loc-color-primary fw-bold ms-2">Description</label>
                    <textarea className="rounded border form-control p-2 mt-1 loc-bg-lay-light" />
                </div>



            </form>


        </div>
        <div className="d-flex flex-wrap mx-2 mt-3">
            <button type="button" className="   rounded me-3 loc-bg-lay-primary text-white px-4 ">
                Save
            </button>
            <button type="button" className="btn rounded   loc-text-light px-4 ">
                Cancel
            </button>
        </div>
    </>
}