import React, { useEffect, useMemo, useState } from "react";
import axios from "axios";
import ProductsCard from "../components/products_card";
import { useNavigate, useLocation } from "react-router-dom";
export default function Dashboard() {

    const [categories, setCategory] = useState([])
    const [products, setProducts] = useState([])
    const [selectedCategory, setSelectedCategory] = useState(1)
    const [search, setSearch] = useState("")
    const navigate = useNavigate();
    const location = useLocation();
    const getCategory = () => {
        axios.get('/json/categories.json')
            .then((data) => {
                setCategory(data.data)

            })
            .catch((error) => console.error('Error posting data:', error));
    }

    const getProducts = () => {
        axios.get('/json/products.json'
        )
            .then((data) => {
                setProducts(data.data)

            })
            .catch((error) => console.error('Error posting data:', error));
    }

    useEffect(() => {
        getCategory();
        getProducts();
        // setCategory(["All", "Food", "Drinks", "Snakes", "Packages"])

    }, [])

    useEffect(() => {
        const queryParams = new URLSearchParams(location.search);
        setSelectedCategory(v => parseInt(isNaN(parseInt(queryParams.get("category"))) ? 1 : parseInt(queryParams.get("category"))));
        setSearch(v => queryParams.get("search") ?? v);

    }, [location.search])

    const pushParams = (id, search) => {
        const queryParams = new URLSearchParams({ category: id, search });
        navigate({ search: queryParams.toString() })
    }





    let filterData = useMemo(() => {
        if (selectedCategory === 1) {
            return products.reduce((groups, product) => {
                if (product.name.toLowerCase().includes(search.toLowerCase())) {
                    const categoryId = product.category_id;
                    if (!groups[categoryId]) {
                        groups[categoryId] = [];
                    }
                    groups[categoryId].push(product);
                }
                return groups;
            }, {})
        }
        else {
            return { [selectedCategory]: products?.filter(v => v.category_id === selectedCategory && v.name.toLowerCase().includes(search.toLowerCase())) }
        }


    }, [selectedCategory, products, search])

    return <>
        <div className="m-3  py-3 bg-white shadow rounded">

            <div className="loc-bg-lay-light border d-flex align-items-center  mx-2 rounded py-1 px-2 mx-md-4 ">
                <img src="/icons/search.svg" alt="search" />
                <input onChange={(e => {

                    pushParams(selectedCategory, e.target.value);
                })} className=" col  loc-bg-lay-light   border-0 outline-none " placeholder="seach here ..." />
            </div>
            <div className="d-flex flex-wrap">
                {
                    categories?.map((value) => <div role="button" onClick={() => pushParams(value.id, search)} key={value.id} className={`col-lg cursor-pointer col-md-6 col-12  border-4 border-bottom ${selectedCategory === value.id ? "border-primary" : ""}`}>
                        <p className="loc-color-primay loc-text-medium fw-bold p-0 m-0 px-2 py-3 text-center px-md-3"> {value.name}</p>
                    </div>
                    )
                }
            </div>


            <div>
                <div>
                    {Object.keys(filterData)?.length > 0 ? Object.keys(filterData).map((categoryId) => (
                        <div key={categoryId} className=" mx-2  mx-md-3 mt-3">
                            <h2 className=" loc-text-xxlarge loc-text-primary m-0 p-0 fw-bold">{categories.find(v => v.id === parseInt(categoryId))?.name}</h2>
                            <div className="row ">
                                {filterData[categoryId].length > 0 ? filterData[categoryId].map((product) => (
                                    <div className="col-12 col-md-6 col-lg-4 mt-3" key={product.id}>
                                        <ProductsCard productDetails={product} />

                                        {/* Render other product details here */}
                                    </div>
                                )) :

                                    <p className="text-center fw-bold  text-secondary my-3">
                                        No product found
                                    </p>
                                }
                            </div>
                        </div>
                    )) : <p className="text-center fw-bold  text-secondary my-3">
                        No product found
                    </p>}
                </div>
            </div>
        </div>
    </>
}